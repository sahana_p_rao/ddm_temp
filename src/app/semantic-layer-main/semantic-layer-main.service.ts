import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SemanticLayerMainService {

  constructor(private http:HttpClient) { }

  public handleError(error:any):any{
    let errObj:any = {
      status:error.status
    };
 
    throw errObj;
  }

  public saveTableName(options){

    let serviceUrl = "http://localhost:8000/semantic_layer/table_rename/";


    let requestBody = new FormData();
    requestBody.append('table_id',options.table_id);
    requestBody.append('table_name',options.table_name);

    // console.log(serviceUrl,'serviceUrl'); 
    return this.http.post(serviceUrl, requestBody)
    .pipe(
      catchError(this.handleError)
    );  
    
  };
}
