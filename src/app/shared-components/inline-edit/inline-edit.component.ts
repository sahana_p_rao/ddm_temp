import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-inline-edit',
  templateUrl: './inline-edit.component.html',
  styleUrls: ['./inline-edit.component.css']
})
export class InlineEditComponent implements OnInit {
  
  @Input() item: any;
  @Input() itemID:any;
  @Output() public onSave = new EventEmitter();
  
  isReadOnly = true;  

  ngOnInit() {
    
  }

  onDblClick() {
    this.isReadOnly = !this.isReadOnly;
    console.log('inside dblclick');
  }

  onChange(tableID,tableName){
   
    console.log('inside onchange');
    
  }

  onKeyDown(event,tableID,tableName){
    
    console.log('inside key down');
    
    // if enter key is pressed
    if(event.key === 'Enter'){ 
      this.onSave.emit({table_id:tableID,table_name:tableName});
      this.isReadOnly = true;
    }
  }

}
